var express = require('express');
var routes = require('./routes/routes');
var config = require('./configs/config');

var mongoose = require('mongoose');

var server = express();

mongoose.connect(config.mongoConnectionString ,{ useNewUrlParser: true },function checkDb(error) {
    if(error){
        console.log("error connecting to DB");
    }else{
        console.log("successfully connected to DB");
    }
});


server.use(express.json());
server.use(routes);



server.listen(config.port,function check(error) {
    if (error) {
        console.log("Error...!!");
    } else {
        console.log("Server stated");
    }
});




