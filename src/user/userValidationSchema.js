
var joi = require('joi');

var userSchema = joi.object().keys({
    first_name: joi.string().required(),
    last_name: joi.string().required(),
    email: joi.string().email().required(),
    password: joi.string().required()
});


var validateUserBody = (req, res, next) =>{

    var result = userSchema.validate(req.body);

    console.log(result.error);

    if (result.error) {
        res.send({status: false, message: result.error.details[0].message});
    } else {
        next();
    }
}


module.exports = { validateUserBody };