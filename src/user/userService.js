var userModel = require('./userModel');
var config = require('../../configs/config');
var jwtService = require('../../commonServices/tokenService');


var key = config.encryptorSecretKey;
var encryptor = require('simple-encryptor')(key);

module.exports.getDataFromDBService = () => {


    return new Promise(function myFn(resolve, reject) {

        userModel.find({}, function returnData(error, result) {

            if (error) {
                reject(false);
            } else {
                console.log("******userData");
                console.log(error);
                console.log(result);
                console.log("******userData");
                resolve(result);
            }
        });

    });

}




module.exports.cretaeUserDBService = (userDetais) => {


    return new Promise(function myFn(resolve, reject) {

        var userModelData = new userModel();

        userModelData.first_name = userDetais.first_name;
        userModelData.last_name = userDetais.last_name;
        userModelData.email = userDetais.email;

        var encrypted = encryptor.encrypt(userDetais.password);
        userModelData.password = encrypted;

        userModelData.save(function resultHandle(error, result) {

            if (error) {
                reject(false);
            } else {
                resolve(true);
            }
        });

    });

}


module.exports.loginUserDBService = (userDetais) => {

    return new Promise(function myFn(resolve, reject) {

        userModel.findOne({ email: userDetais.email }, function getResult(errorValue, result) {

            if (errorValue) {
                reject({ status: false, msg: 'Invalid data' });
            } else {
                if (result != undefined && result != null) {

                    var decrypted = encryptor.decrypt(result.password);
                    if (decrypted == userDetais.password) {
                        var token = jwtService.createToken(result);
                        var encryptedToken = encryptor.encrypt(token);


                        resolve({ status: true, msg: encryptedToken });
                    } else {
                        resolve({ status: false, msg: 'Invalid user password' });
                    }

                } else {
                    resolve({ status: false, msg: 'Invalid user details' });
                }

            }

        });


    });

}





// module.exports = { getDataFromDBService };








