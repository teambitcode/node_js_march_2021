var userService = require('./userService');




var getDataControllerFn = async (req, res) => {

    var empDetails = await userService.getDataFromDBService();
    res.send({ "status": true, "data": empDetails });

}


var createUserControllerFn = async (req, res) => {

    console.log(req.body);

    var status = await userService.cretaeUserDBService(req.body);
    console.log("*************************");
    console.log(status);
    console.log("*************************");

    if (status) {
        res.send({ "status": true, "message": "User created successfully" });
    } else {
        res.send({ "status": false, "message": "Error creating user" });
    }


}


var loginUserControllerFn = async (req, res) => {
    var result = null;
    try {


        result = await userService.loginUserDBService(req.body);


        if (result.status) {
            res.send({ "status": true, "message": result.msg });
        } else {
            res.send({ "status": false, "message": result.msg });
        }

    } catch (error) {
        console.log(error);
        res.send({ "status": false, "message": error.msg });
    }



}



module.exports = { getDataControllerFn, createUserControllerFn, loginUserControllerFn };