var productService = require('./productService');



var createProductControllerFn = async (req, res) => {

    var status = await productService.cretaeProductDBService(req.body);

    if (status) {
        res.send({ "status": true, "message": "Product created successfully" });
    } else {
        res.send({ "status": false, "message": "Error creating product" });
    }

}

var findOneProductControllerFn = async (req, res) => {

    console.log(req.query);

    var result = await productService.findOneProductDBService(req.query);

    if (result) {
        res.send({ "status": true, "data":result});
    } else {
        res.send({ "status": false, "data": "Product not found" });
    }

}

var updateProductControllerFn = async (req, res) => {

    console.log(req.params.id);
    console.log(req.body);

    var result = await productService.updateOneProductDBService(req.params.id,req.body);

    if (result) {
        res.send({ "status": true, "message":"Product updated successfuly"});
    } else {
        res.send({ "status": false, "data": "Product fail to update" });
    }

}

var deleteProductControllerFn = async (req, res) => {

    console.log(req.params.id);


    var result = await productService.removeOneProductDBService(req.params.id);

    if (result) {
        res.send({ "status": true, "message":"Product deleted successfuly"});
    } else {
        res.send({ "status": false, "data": "Product fail to delete" });
    }

}


module.exports = { createProductControllerFn, findOneProductControllerFn, updateProductControllerFn, deleteProductControllerFn };