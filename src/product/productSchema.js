var joi = require('joi');

var productCreateSchema = joi.object().keys({
    name: joi.string().required(),
    price: joi.number().required(),
    quntity: joi.number().required()
});

var productUpdateSchema = joi.object().keys({
    name: joi.string(),
    price: joi.number(),
    quntity: joi.number()
});


var validateCreateProductBody = (req, res, next) =>{

    var result = productCreateSchema.validate(req.body);

    console.log(result.error);

    if (result.error) {
        res.send({status: false, message: result.error.details[0].message});
    } else {
        next();
    }
}

var validateUpdateProductBody = (req, res, next) =>{

    var result = productUpdateSchema.validate(req.body);

    console.log(result.error);

    if (result.error) {
        res.send({status: false, message: result.error.details[0].message});
    } else {
        next();
    }
}



module.exports = { validateCreateProductBody, validateUpdateProductBody };
