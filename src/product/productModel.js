var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var productSchema = new Schema({

    name: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    quntity: {
        type: Number,
        required: true
    }

});



module.exports = mongoose.model('products', productSchema);