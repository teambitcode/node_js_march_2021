var jwt = require('jsonwebtoken');
var configs = require('../configs/config');

var key = configs.encryptorSecretKey;
var encryptor = require('simple-encryptor')(key);




/**
 * 
 * @param {*} reqest 
 * @param {*} res 
 * @param {*} netxFun1 
 */
var verifyJwtToken = (reqest, res, netxFun1) => {
    console.log("test request 1");
    console.log(reqest.headers.authorization);

    var decryptedToken = encryptor.decrypt(reqest.headers.authorization);

    jwt.verify(decryptedToken, configs.jwtKey, function tokenResult(error, result) {

        if (error) {
            res.send("Invalid token");
        } else {
            console.log(result);
            netxFun1();
        }

    });
}



var createToken = (userDetailsObject)=> {
    console.log("userDetailsObject");
    console.log(userDetailsObject);
    console.log("userDetailsObject");
    
    var userDetails = {
        email: userDetailsObject.email,
        first_name: userDetailsObject.first_name,
        last_name: userDetailsObject.last_name
    };
    
        var jwtToken = jwt.sign(userDetails, configs.jwtKey, { expiresIn: configs.expireTime });
    
        return jwtToken;
    }

module.exports = { verifyJwtToken, createToken };