// Import Express
var express = require('express');

var jwtService = require('../commonServices/tokenService');
var userController = require('../src/user/userController');
var productController = require('../src/product/productController');
var userValidationSchema = require('../src/user/userValidationSchema');
var productValidationSchema = require('../src/product/productSchema');

// Import Router
var router = express.Router();

// user routes
router.route('/user/getAll').get(jwtService.verifyJwtToken, userController.getDataControllerFn);
router.route('/user/create').post(jwtService.verifyJwtToken,userValidationSchema.validateUserBody,userController.createUserControllerFn);
router.route('/user/login').post(userController.loginUserControllerFn);


// product route
router.route('/product/create').post(productValidationSchema.validateCreateProductBody,productController.createProductControllerFn);
router.route('/product/findOne').get(productController.findOneProductControllerFn);
router.route('/product/update/:id').patch(productValidationSchema.validateUpdateProductBody ,productController.updateProductControllerFn);
router.route('/product/remove/:id').delete(productController.deleteProductControllerFn);


module.exports = router;